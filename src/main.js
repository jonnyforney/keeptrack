import Vue from 'vue'
import router from './router'

//  import dependencies
import jquery from 'jquery'
import swal from 'sweetalert2/dist/sweetalert2.js'

import 'sweetalert2/src/sweetalert2.scss'
import 'skeleton-css/css/skeleton.css'
import marked from 'marked'

import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

import config from './firebase/config.js'

let db = firebase.initializeApp(config)

window.swal = swal
window.moment = require('moment')
window.timeformat = 'MMMM DD, YYYY hh:mm A'
window.Vue = Vue
window.$ = jquery
window.fb = firebase
window.db = db
window.marked = marked

require('./classes/Event')
require('./helpers/filters')

//  import components
import App from './App.vue'

Vue.config.productionTip = false

/* eslint-disable no-new */
var KEEPTRACK = new Vue({
    el: '#app',
    template: '<App/>',
    router,
    components: { App }
})