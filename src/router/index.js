import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/auth/Login.vue'
// import Signup from '@/components/auth/Signup.vue'
import Journal from '@/components/Journal.vue'

Vue.use(Router)

export default new Router({
    routes: [
        { name: 'login', path: '/login', component: Login },
        // { path: '/signup', component: Signup },
        { name: 'journal', path: '/journal', component: Journal }
    ]
})